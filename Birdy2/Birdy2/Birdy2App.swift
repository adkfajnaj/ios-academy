//
//  Birdy2App.swift
//  Birdy2
//
//  Created by Student on 13.12.2023..
//

import SwiftUI

@main
struct Birdy2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
